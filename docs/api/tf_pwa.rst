tf\_pwa package
===============


Module contents
---------------

.. automodule:: tf_pwa
   :members:
   :undoc-members:
   :show-inheritance:


Submodules
----------

.. tf\_pwa.version module
.. ----------------------
.. 
.. .. automodule:: tf_pwa.version
..    :members:
..    :undoc-members:
..    :show-inheritance:


tf\_pwa.config module
------------------------

.. automodule:: tf_pwa.config
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.config\_loader module
-----------------------------

.. automodule:: tf_pwa.config_loader
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: tf_pwa.config_loader.config_loader
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: tf_pwa.config_loader.decay_config
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.cg module
-----------------

.. automodule:: tf_pwa.cg
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.dfun module
-----------------------

.. automodule:: tf_pwa.dfun
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.breit\_wigner module
----------------------------

.. automodule:: tf_pwa.breit_wigner
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.data module
------------------------

.. automodule:: tf_pwa.data
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.angle module
--------------------

.. automodule:: tf_pwa.angle
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.cal_angle module
------------------------

.. automodule:: tf_pwa.cal_angle
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.particle module
-------------------------

.. automodule:: tf_pwa.particle
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.variable module
-----------------------

.. automodule:: tf_pwa.variable
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.amp module
---------------------

.. automodule:: tf_pwa.amp
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.model module
--------------------

.. automodule:: tf_pwa.model
   :members:
   :imported-members:
   :show-inheritance:

.. automodule:: tf_pwa.model.cfit
   :members:
   :show-inheritance:


tf\_pwa.utils module
--------------------

.. automodule:: tf_pwa.utils
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.applications module
---------------------------

.. automodule:: tf_pwa.applications
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.phasespace module
-------------------------

.. automodule:: tf_pwa.phasespace
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.einsum module
------------------------

.. automodule:: tf_pwa.einsum
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.vis module
------------------------

.. automodule:: tf_pwa.vis
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.significance module
---------------------------

.. automodule:: tf_pwa.significance
   :members:
   :undoc-members:
   :show-inheritance:

tf\_pwa.gpu\_info module
------------------------

.. automodule:: tf_pwa.gpu_info
   :members:
   :undoc-members:
   :show-inheritance:
